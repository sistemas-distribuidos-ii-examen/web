import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

//Animaciones
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

//Compartir informacion entre componentes
import { SharingDataService } from "./sharingData.service";

//Para poder hacer peticiones HTTP a la API
import { HttpClientModule } from "@angular/common/http";
import { ApiHTTP } from "./http.service";

//Enrutador 
import { RouterModule, Route } from '@angular/router';

//Angular
import { FormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input'
import { MatSelectModule } from '@angular/material/select'
import { MatDividerModule } from '@angular/material/divider'
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatButtonModule} from '@angular/material/button'
import {MatNativeDateModule} from '@angular/material/core';

//Leaftlet
import { LeafletModule } from '@asymmetrik/ngx-leaflet';

//Componentes
import { AppComponent } from './app.component';

import { Home } from './home/home.component';
import { Certificado } from './home/certificado/certificado.component';
import { Admin } from './admin/admin.component';

const routes: Route[] = [
	{
		path: '',
		component: Home,
		children:[{
			path:'certificado',
			component:Certificado	
		}]
	},
	{
		path: 'admin',
		component: Admin
	},
	{
		path: '**',
		redirectTo: '/welcome',
		pathMatch: 'full'
	}
];

@NgModule({
	declarations: [
		AppComponent,
		Home,
		Certificado,
		Admin
	],
	imports: [
		BrowserModule, //Este siempre se importa primero, por precaucion de errores
		BrowserAnimationsModule,
		RouterModule.forRoot(routes),
		HttpClientModule,
		FormsModule,
		MatInputModule,
		MatSelectModule,
		MatDividerModule,
		MatDatepickerModule,
		MatButtonModule,
		MatNativeDateModule,
		LeafletModule
	],
	providers: [
		SharingDataService,
		ApiHTTP,
	],
	bootstrap: [AppComponent]
})
export class AppModule{ }
