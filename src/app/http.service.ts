import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../environments/environment';


@Injectable()
export class ApiHTTP {

    private api_url:string;
    private api_geo_ref_url:string;


    constructor(public http:HttpClient){
        this.api_url = environment.api_url;
        this.api_geo_ref_url= environment.api_geo_ref_url;
    }

    public get(condicion:string,headers:any){
        return this.http.get(this.api_url+condicion,headers);
    }

    public getGeoRef(condicion:string,headers:any){
        return this.http.get(this.api_geo_ref_url+condicion,headers);
    }

    public postGeoRef(condicion:string,data:any,headers:any){
        return this.http.post(this.api_geo_ref_url+condicion,headers);
    }

    public post(condicion:string,data:any,headers:any){
        return this.http.post(this.api_url+condicion,data,headers);
    }

}
