import { Component, OnInit } from '@angular/core';

//Compartir informacion entre componentes
import { SharingDataService } from "../sharingData.service";
import { Subscription } from "rxjs";

//Para llamadas a la API
import { ApiHTTP } from "../http.service";

//Para poder cambiar el enrutamiento mediante Typescript sin que muera el SPA y administrar paramentos de ruta
import { Router } from '@angular/router';

@Component({
  selector: 'home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class Home implements OnInit{
  //Vista
  view_home: Boolean = false;

  //Atributos
  dataSubscription: Subscription;
  data: Array<string> = [];

  //Atributos Geo Ref
  provincias: Array<string> = [];
  departamentos: Array<string> = [];
  localidades: Array<string> = [];
  ciudadanos: Array<string> = [];

  
  //Manejador de partes disabled/enabled
  disabled={
    "departamentos":true,
    "localidades":true
  }

  //Campos para el manejo de peticiones
  campos={
    "ciudadano":{
      "data":{
        type:"Ciudadano",
        id:null,
        "attributes":{
          cuil:"",
          nombre:"",
          apellido:"",
          nro_doc:"",
          nro_tramite:"",
          telefono:"",
          email:"",
          sexo:""
        },
        "relationships": {
          "permiso_set": {
              "data": [],
              "meta": {
                  count: 0
              }
          }
        }
      }
    },
    "domicilio":{
      "data":{
        type:"Domicilio",
        id:null,
        "attributes":{
          id_provincia:"",
          id_departamento:"",
          id_localidad:"",
          domicilio:"",
          lat_localidad:"",
          long_localidad:""
        },
        "relationships": {
          "permiso_set": {
              "data": [],
              "meta": {
                  count: 0
              }
          }
        }
      }
    },
    "permiso":{
      "data":{
        type:"Permiso",
        id:null,
        "attributes":{
          fecha_vigencia:"",
          domicilio:"http://localhost:8000/domicilios/",
          ciudadano:"http://localhost:8000/ciudadanos/"
        }
      }
    }
  }

  //Campos auxiliares 
  auxiliar={
    ciudadano:{},
    email:"",
    fecha_vigencia_date: new Date("mm/dd/yyy"),
    localidad:{}
  }

  //Atributo para manejar errores
  errores={
    nombre: false,
    apellido: false,
    nro_doc: false,
    cuil: false,
    sexo: false,
    fecha_vigencia: false,
    nro_tramite:false,
    id_provincia:false,
    id_departamento:false,
    id_localidad:false,
    coincidencia_correo:false
  }

  //Constructor
  constructor(public _sharingDataService:SharingDataService, public _router:Router, public http: ApiHTTP){
    console.log("Ejecucion --> Constructor Componente Home");
  }

  //Metodos
  public ngOnInit() {
    console.log("Ejecucion --> ngOnInit Componente Home");
    this.dataSubscription = this._sharingDataService.dataSource$.subscribe(data => { this.data = data; });
    
    this.getProvincias();
    this.view_home=true;
  }

  //Metodos Geo Ref

  public getProvincias(){
    this.http.getGeoRef("/provincias", this.data['httpOptions']).subscribe(result => {
      if(result["cantidad"]){
          this.provincias=result["provincias"];
      }else{
          console.log("Error ",result['mensaje']);
      }
    },error => {
        console.log('Ah ocurrido un error');
    })
  }

  public getDepartamentos(event,id_provincia){
    if(event.isUserInput){
      this.http.getGeoRef("/departamentos?provincia="+id_provincia+"&max=500", this.data['httpOptions']).subscribe(result => {
        if(result["cantidad"]){
            this.departamentos=result["departamentos"];
            this.disabled.departamentos=false;
        }else{
            console.log("Error ",result['mensaje']);
        }
      },error => {
          console.log('Ah ocurrido un error');
      })
    }
  }

  public getLocalidades(event,id_departamento){
    if(event.isUserInput){
      this.http.getGeoRef("/localidades?departamento="+id_departamento+"&max=500", this.data['httpOptions']).subscribe(result => {
        if(result["cantidad"]){
            this.localidades=result["localidades"];
            this.disabled.localidades=false;
        }else{
            console.log("Error ",result['mensaje']);
        }
      },error => {
          console.log('Ah ocurrido un error');
      })
    }
  }

  //Verificar mail y su confirmacion

  public verificarMail(){
    if (this.campos.ciudadano.data.attributes.email!=this.auxiliar.email){
      this.errores.coincidencia_correo=true;
    }else{
      this.errores.coincidencia_correo=false;
    }
  }

  public registrar(){
    console.log("registrar()");

    this.campos.permiso.data.attributes.ciudadano="http://localhost:8000/ciudadanos/"
    this.campos.permiso.data.attributes.domicilio="http://localhost:8000/domicilios/"
    let error=false;

    
    //Errores datos ciudadano
    if(this.campos.ciudadano.data.attributes.nombre==""){
      this.errores.nombre=true;
      error=true;
    }
    if(this.campos.ciudadano.data.attributes.apellido==""){
      this.errores.apellido=true;
      error=true;
    }
    if(this.campos.ciudadano.data.attributes.nro_doc==""){
      this.errores.nro_doc=true;
      error=true;
    }
    if(this.campos.ciudadano.data.attributes.cuil==""){
      this.errores.cuil=true;
      error=true;
    }
    if(this.campos.ciudadano.data.attributes.nro_tramite==""){
      this.errores.nro_tramite=true;
      error=true;
    }
    if(this.campos.ciudadano.data.attributes.sexo==""){
      this.errores.sexo=true;
      error=true;
    }

    //Errores datos domicilio

    if(this.campos.domicilio.data.attributes.id_localidad==""){
      this.errores.id_localidad=true;
      error=true;
    }
    if(this.campos.domicilio.data.attributes.id_provincia==""){
      this.errores.id_provincia=true;
      error=true;
    }
    if(this.campos.domicilio.data.attributes.id_departamento==""){
      this.errores.id_departamento=true;
      error=true;
    }

    //Errores datos permiso
    
    if(this.campos.permiso.data.attributes.fecha_vigencia==""){
      this.errores.fecha_vigencia=true;
      error=true;
    }
    
    
    if(!error && !this.errores.coincidencia_correo){
      //Buscar en el array de localidades la latitud y longitud
      this.auxiliar.localidad=this.localidades.find( localidad => localidad['id'] === this.campos.domicilio.data.attributes.id_localidad);
      this.campos.domicilio.data.attributes.lat_localidad=this.auxiliar.localidad['centroide']['lat'];
      this.campos.domicilio.data.attributes.long_localidad=this.auxiliar.localidad['centroide']['lon'];

      //Post
      this.generarCertificado();
    }
  }

  public generarCertificado(){
    console.log("generarCertificado()");
    if(this.ciudadanos.length===0){
      this.getCiudadanos();
    }else{
      this.buscarCiudadano();
    }
  }

  public buscarCiudadano(){
    console.log("buscarCiudadano()");
    this.auxiliar.ciudadano=this.ciudadanos.find( ciudadano => ciudadano['attributes']['cuil'] === this.campos.ciudadano.data.attributes.cuil);
    if(this.auxiliar.ciudadano!=undefined){
      this.campos.permiso.data.attributes.ciudadano=this.campos.permiso.data.attributes.ciudadano+this.auxiliar.ciudadano['id']+"/";
      this.postDomicilio();
    }else{
      this.postCiudadano();
    }
  }

  public getCiudadanos(){
    console.log("getCiudadanos()");
    this.http.get("/ciudadanos/", this.data['httpOptions_api']).subscribe(result => {
      this.ciudadanos=result['data'];
      if(this.ciudadanos.length==0){
        this.postCiudadano();  
      }else{
        this.auxiliar.ciudadano=this.ciudadanos.find( ciudadano => ciudadano['attributes']['cuil'] === this.campos.ciudadano.data.attributes.cuil);
        if(this.auxiliar.ciudadano!=undefined){
          this.campos.permiso.data.attributes.ciudadano=this.campos.permiso.data.attributes.ciudadano+this.auxiliar.ciudadano['id']+"/";
          this.postDomicilio();
        }else{
          this.postCiudadano();
        } 
      }
      
    },error => {
        console.log('Ah ocurrido un error');
    })
  }

  public cambioFecha(event){
    let fecha_analisador = event.targetElement.value.split("/");
    if(fecha_analisador[0].length==1){
        fecha_analisador[0] = "0"+fecha_analisador[0];
    }
    if(fecha_analisador[1].length==1){
        fecha_analisador[1] = "0"+fecha_analisador[1];
    }
    this.campos.permiso.data.attributes.fecha_vigencia = fecha_analisador[2]+"-"+fecha_analisador[0]+"-"+fecha_analisador[1]+ " 00:00";
    this.auxiliar.fecha_vigencia_date = new Date(fecha_analisador[0]+"-"+fecha_analisador[1]+"-"+fecha_analisador[2]);
  }

  public postCiudadano(){
    console.log("this.postCiudadano()");
    this.http.post("/ciudadanos/",this.campos.ciudadano, this.data['httpOptions_api']).subscribe(result => {
      if(result['data']){
        this.campos.permiso.data.attributes.ciudadano=this.campos.permiso.data.attributes.ciudadano+result['data']['id']+"/";
        this.auxiliar.ciudadano=result['data'];
        this.postDomicilio();
      }
    },error => {
        console.log('Ah ocurrido un error');
    })
  }

  public postDomicilio(){
    console.log("this.postDomicilio()");
    this.http.post("/domicilios/",this.campos.domicilio, this.data['httpOptions_api']).subscribe(result => {
      if(result['data']){
        this.campos.permiso.data.attributes.domicilio=this.campos.permiso.data.attributes.domicilio+result['data']['id']+"/";
        this.postPermiso();
      }
    },error => {
        console.log('Ah ocurrido un error');
    })
  }

  public postPermiso(){
    console.log("this.postPermiso()");
    this.http.post("/permisos/",this.campos.permiso, this.data['httpOptions_api']).subscribe(result => {
      this.data['campos']={
        nombre:this.auxiliar.ciudadano["attributes"]["nombre"],
        apellido:this.auxiliar.ciudadano["attributes"]["apellido"],
        nro_doc:this.auxiliar.ciudadano["attributes"]["nro_doc"],
        localidad:this.auxiliar.localidad["nombre"],
        departamento:this.auxiliar.localidad["departamento"]["nombre"],
        provincia:this.auxiliar.localidad["provincia"]["nombre"],
        fecha_vigencia:this.campos.permiso.data.attributes.fecha_vigencia.split(" ")[0],
        id_permiso:result['data']['id']
      }
      this._sharingDataService.setData(this.data);
      this.view_home=false;
      this._router.navigate(['/certificado']);
    },error => {
        console.log('Ah ocurrido un error');
    })
  }

  public ngOnDestroy(): void {
    console.log("Destruccion --> ngOnDestroy Componente Home");
    this.dataSubscription.unsubscribe();
  }
}
