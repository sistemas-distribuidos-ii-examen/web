import { Component, OnInit } from '@angular/core';

//Compartir informacion entre componentes
import { SharingDataService } from "../../sharingData.service";
import { Subscription } from "rxjs";

//Para llamadas a la API
import { ApiHTTP } from "../../http.service";

//Para poder cambiar el enrutamiento mediante Typescript sin que muera el SPA y administrar paramentos de ruta
import { Router } from '@angular/router';

@Component({
  selector: 'certificado',
  templateUrl: './certificado.component.html',
  styleUrls: ['./certificado.component.css']
})
export class Certificado implements OnInit{
    //Atributos
    dataSubscription: Subscription;
    data: Array<string> = [];

    //Constructor
    constructor(public _sharingDataService:SharingDataService, public _router:Router, public http: ApiHTTP){
        console.log("Ejecucion --> Constructor Componente Certificado");
    }

    //Metodos
    public ngOnInit() {
        console.log("Ejecucion --> ngOnInit Componente Certificado");
        this.dataSubscription = this._sharingDataService.dataSource$.subscribe(data => { this.data = data; });
        if(this.data['campos']==undefined){
            this._router.navigate(['']);
        }else{
            console.log(this.data['campos'])
        }
    }

    public ngOnDestroy(): void {
        console.log("Destruccion --> ngOnDestroy Componente Certificado");
        this.dataSubscription.unsubscribe();
    }
}
