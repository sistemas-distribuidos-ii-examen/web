import { Component, OnInit } from '@angular/core';

//Para poder cambiar el enrutamiento mediante Typescript sin que muera el SPA
import { Router } from '@angular/router';

//Compartir informacion entre componentes
import { SharingDataService } from "./sharingData.service";
import { Subscription } from "rxjs";

//Para los Headers y Urls de la API
import { HttpHeaders } from '@angular/common/http';
import { ApiHTTP } from "./http.service";

//Configuracion general de la web
import { environment } from '../environments/environment';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit {

    //Atributos
    dataSubscription: Subscription;
    data: Array<string> = []; // Variables compartida(entre componentes), contiene informacion general.
    session_view: boolean = false;

    //Constructor
    constructor (public _sharingDataService:SharingDataService,public _router:Router,public http: ApiHTTP){
        console.log("Ejecucion --> Constructor Componente AppComponent");
        //Configuracion general
        this.data['httpOptions'] = { headers: new HttpHeaders({ 'Content-Type': 'application/json' })};
        this.data['httpOptions_api'] = { headers: new HttpHeaders({ 'Content-Type': 'application/vnd.api+json' })};
        this.data['web_url'] = environment.web_url;
        this.data['web_name'] = environment.web_name;
        this.data['version'] = environment.version;
        this.data['title_num'] = "";
        this.data['title_name'] = this.data['web_name'];
        //------------------------------
        this._sharingDataService.setData(this.data);
    }

    //Metodos
    public ngOnInit() {
        console.log("Ejecucion --> ngOnInit Componente AppComponent");
        this.dataSubscription = this._sharingDataService.dataSource$.subscribe(data => { this.data = data; });
    }

    public ngOnDestroy(): void {
        console.log("Destruccion --> ngOnDestroy Componente AppComponent");
        this.dataSubscription.unsubscribe();
    }
    
}

