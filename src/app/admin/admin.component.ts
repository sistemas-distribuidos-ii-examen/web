import { AfterViewInit, Component, OnInit } from '@angular/core';

//Compartir informacion entre componentes
import { SharingDataService } from "../sharingData.service";
import { Subscription } from "rxjs";

//Para llamadas a la API
import { ApiHTTP } from "../http.service";

//Para leaflet
import * as L from 'leaflet';

//Para poder cambiar el enrutamiento mediante Typescript sin que muera el SPA y administrar paramentos de ruta
import { Router } from '@angular/router';

@Component({
  selector: 'admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class Admin implements OnInit,AfterViewInit{
    //Atributos
    dataSubscription: Subscription;
    data: Array<string> = [];

    puntos: Array<any> = [];
    permisos: Array<string> = [];
    permisos_today: Array<string> = [];

    //Config Map
    private map;

    private initMap(): void {
        this.map = L.map('map', {
          center: [ -38.8367814, -74.3859482 ],
          zoom: 4
        });
    
        const tiles = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
          maxZoom: 18,
          minZoom: 3,
          attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
        });
    
        tiles.addTo(this.map);
    }

    //Constructor
    constructor(public _sharingDataService:SharingDataService, public _router:Router, public http: ApiHTTP){
        console.log("Ejecucion --> Constructor Componente Admin");
    }

    //Metodos
    public ngOnInit() {
        console.log("Ejecucion --> ngOnInit Componente Admin");
        this.dataSubscription = this._sharingDataService.dataSource$.subscribe(data => { this.data = data; });
    
        this.getPermisos();
    }

    ngAfterViewInit(): void {
        this.initMap();
    }

    public getFechaActual():String{
        console.log("getFechaActual()");

        let fecha_actual= new Date();
        let fecha=""

        fecha=fecha_actual.getFullYear()+"-";

        if(fecha_actual.getMonth()+1<10){
            fecha=fecha+"0"+(fecha_actual.getMonth()+1).toString()+"-";
        }else{
            fecha=(fecha_actual.getMonth()+1).toString()+"-";
        }

        if(fecha_actual.getUTCDate()<10){
            fecha=fecha+"0"+fecha_actual.getUTCDate()
        }else{
            fecha=fecha+fecha_actual.getUTCDate()
        }

        return fecha
    }

    public getDomicilio(id:string){
        console.log("this.getDomicilio("+id+")");
        this.http.get("/domicilios/"+id+"/", this.data['httpOptions_api']).subscribe(result => {
            let domicilio=result['data'];
            console.log(domicilio);
            let punto=L.circleMarker(L.latLng(domicilio['attributes']['lat_localidad'],domicilio['attributes']['long_localidad']));
            if(!this.puntos.includes(punto)){
                punto.addTo(this.map)
                this.puntos.push(punto);
            }else{
                /*Aqui se podria hacer una proporcion del elemento radius del marker
                en base a la cantidad de puntos repetidos en el array*/ 
            }
            
        },error => {
            console.log('Ah ocurrido un error');
        })
    }

    public getPermisos(){
        console.log("getPermisos()");
        this.http.get("/permisos/", this.data['httpOptions_api']).subscribe(result => {
            this.permisos=result['data'];
            let fecha_actual=this.getFechaActual();
            this.permisos_today=this.permisos.filter( permiso => permiso['attributes']['fecha_vigencia'].split("T")[0] === fecha_actual);
            for (let permiso of this.permisos_today) {
                this.getDomicilio(permiso['id'])
            }
        },error => {
            console.log('Ah ocurrido un error');
        })
      }

    public ngOnDestroy(): void {
        console.log("Destruccion --> ngOnDestroy Componente Admin");
        this.dataSubscription.unsubscribe();
    }
}
